import os
import json
import requests
from bs4 import BeautifulSoup
import threading
from urllib.request import urlretrieve
import sys

class downTh(threading.Thread):
    def __init__(self, thId, totalId, tsList):
        threading.Thread.__init__(self)
        
        self.thId = thId
        num = len(tsList)//totalId
        if thId==totalId-1:
            self.tsList = tsList[num*self.thId:len(tsList)]
        
        else:
            self.tsList = tsList[num*self.thId:num*(self.thId+1)]
    
    def run(self):
        for url in self.tsList:
            urlretrieve(url, url[url.find('seg-'):])
        
        print('Finish %d' % (self.thId))

if len(sys.argv)<2:
    print('Please input the vid')
    exit()
        
prvUrl = 'https://api.avgle.com/v1/video/'
vid = sys.argv[1]
par = json.loads(requests.get(prvUrl+vid).text)
embUrl = par['response']['video']['embedded_url']
fileName = par['response']['video']['title']
print('vid %d is %s' % (int(vid), fileName))
bs = BeautifulSoup(requests.get(embUrl).text, 'html.parser')
m3u8Url = bs.find('source')['src']
m3u8File = (requests.get(m3u8Url).text).split('\n')

tsList = []
for line in m3u8File:
    if not line.endswith('.ts'):
        continue
    
    tsList.append(line)

ths = []
totalId = 15
for i in range(totalId):
    ths.append(downTh(i, totalId, tsList))
    ths[i].start()

for th in ths:
    th.join()

print('Total Finish')

res = os.system('cat *.ts > all.ts')
res = os.system('ffmpeg -i all.ts -acodec copy -vcodec copy all.mp4')
res = os.system('mv all.mp4 '+'\''+fileName+'\''+'.mp4')
res = os.system('rm -rf *.ts')

