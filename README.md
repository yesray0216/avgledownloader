# AvgleDownloader
This is an efficient downloader of [Avgle](https://avgle.com/).

## Preparation

### Python3
+ BeautifulSoup
+ requests

You can just install them by [pip](https://pip.pypa.io/en/stable/installing/)

	pip install bs4 requests

### Shell
+ FFmpeg

It's also easy to install

	brew install ffmpeg   // osx
	apt-get install ffmpeg // linux

## Usage
Input the vid of your favorite video and run using python

![](http://i.imgur.com/bCVNDaX.png)

	python AvgleDownloader.py 72607

Wait, and have fun!

## Snapshot

![](http://i.imgur.com/aogkOLT.jpg)
